package com.twitter.fernando.db;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterosRepository extends JpaRepository<TwitterosDB, Long>{
    /**
     * Obtiene todos los objetos de la tabla twiteros
     * @return todos los tweets
     */
    @Query("SELECT u FROM TwitterosDB u")
    List<TwitterosDB> findAllTwitteros();

    /**
     * obtiene todo los twits validados por un usuario
     * @param user usuario a buscar
     * @return lista de tweets
     */
    @Query("SELECT u FROM TwitterosDB u WHERE u.validacion = true AND u.usuario = ?1")
    List<TwitterosDB> findAllVerifiedUser(String user);
}
