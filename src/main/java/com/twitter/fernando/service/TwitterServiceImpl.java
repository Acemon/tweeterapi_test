package com.twitter.fernando.service;

import java.util.ArrayList;
import java.util.List;

import com.twitter.fernando.db.TwitterosDB;
import com.twitter.fernando.db.TwitterosRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

@Service
public class TwitterServiceImpl implements TwitterService{

    /**
     * Busqueda a nivel mundial
     */
    private final int WOEID = 1;

    @Value("${limit.subs}")
    private Integer limitSubs;

    @Value("${limit.trends}")
    private Integer limitTrends;

    @Value("#{'${languages}'.split(',')}")
    private List<String> languages;

    @Autowired
    private TwitterosRepository twitterosRepo;

    Logger log = LoggerFactory.getLogger(TwitterServiceImpl.class);

    Twitter twitter = new TwitterFactory().getInstance();

    @Override
    public void guardarTweet(String query) {
        log.info("guardartweet() - Init");
        try{
            Query qry = new Query(query);
            QueryResult result = twitter.search(qry);
            for (Status status : result.getTweets()) {
                if (status.getUser().getFollowersCount() > limitSubs && languages.contains(status.getLang())){
                    TwitterosDB twittero = new TwitterosDB();
                    twittero.setUsuario(status.getUser().getName());
                    twittero.setTexto(status.getText());
                    twittero.setLocalizacion(status.getLang());
                    twittero.setValidacion(false);
                    twitterosRepo.save(twittero);
                }
            } 
        }catch(TwitterException e){
            e.printStackTrace();}
        
        log.info("guardartweet() - Exit");
    }

    @Override
    public List<TwitterosDB> obtenerTweets() {
        //Puede utilizarse findAll() para obtener todos, pero por darle una vuelta de tuerca he hecho una custom Query
        //twiterosRepo.findAll()
        log.info("obtenertweets() - Init");
        List<TwitterosDB> twitteros = twitterosRepo.findAllTwitteros();
        log.info("obtenertweets() - Exit");
        return twitteros;
    }

    @Override
    public List<TwitterosDB> buscarValidados(String user) {
        
        log.info("buscarValidados() - Init");
        List<TwitterosDB> twitteros = twitterosRepo.findAllVerifiedUser(user);
        log.info("buscarValidados() - Exit");
        return twitteros;
    }

    @Override
    public void validateTweet(Long id) {
        log.info("validatetwit() - Init");
        TwitterosDB twittero = twitterosRepo.getById(id);
        twittero.setValidacion(true);
        twitterosRepo.save(twittero);
        log.info("validateTweet() - Exit");
    }

    @Override
    public List<String> buscarTrends() {
        //No he encontrado ningun metodo en la api para recoger una cantidad fija de hashtags, el dafault segun la api es 50
        log.info("buscarTrends() - Init");
        List<String> listaTrends = new ArrayList<>();
        try {
            Trends trends = twitter.getPlaceTrends(this.WOEID);
            for (int i = 0; i < limitTrends; i++){
                listaTrends.add(trends.getTrends()[i].getName());
            }
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        log.info("buscarTrends() - Exit");
        return listaTrends;
    }
}