package com.twitter.fernando.service;

import java.util.List;

import com.twitter.fernando.db.TwitterosDB;

public interface TwitterService {
    
    /**
     * Realiza una busqueda mediante el parametro dado y almacena los tweets en h2
     * @param query
     */
    public void guardarTweet(String query);

    /**
     * Obtiene todos los tweets almacenados en h2
     * @return
     */
    public List<TwitterosDB> obtenerTweets();

    /**
     * Busca todos los tweets validados por usuario
     * @param user usuario a buscar
     * @return lista del objeto que se almacena en bbdd ya validado
     */
    public List<TwitterosDB> buscarValidados(String user);

    /**
     * Valida un tweet almacenado en la bbdd h2
     * @param id id del tweet a validar
     */
    public void validateTweet(Long id);

    /**
     * Metodo para buscar el top trending mundial
     * @return Lista con el top de trendings
     */
    public List<String> buscarTrends();
}
