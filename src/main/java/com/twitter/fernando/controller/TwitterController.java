package com.twitter.fernando.controller;

import java.util.List;

import com.twitter.fernando.db.TwitterosDB;
import com.twitter.fernando.service.TwitterService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TwitterController{

    Logger log = LoggerFactory.getLogger(TwitterController.class);

    @Autowired
    TwitterService twitterService;

    /**
     * Busca y almacena twits en bbdd
     * @param twit busqueda
     * @return 201 Created
     */
    @RequestMapping(value = "/find/{twit}", method = RequestMethod.GET)
    public ResponseEntity<Void> gettwit(@PathVariable String twit){
        twitterService.guardarTweet(twit);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * obtiene todos los twits de h2
     * @return lista de twits 200 OK
     */
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TwitterosDB>> getAllTwits(){
        List<TwitterosDB> response = twitterService.obtenerTweets();
        return new ResponseEntity<List<TwitterosDB>>(response, HttpStatus.OK);
    }

    /**
     * valida un twit por id de bbdd
     * @param id id a validar
     * @return 201 Created
     */
    @RequestMapping(value = "/validateTwit/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> getvalidatedtwits(@PathVariable Long id){
        twitterService.validateTweet(id);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Busca los twits validados por usuario
     * @param user usuario con twits validados
     * @return lista de twits 200 OK
     */
    @RequestMapping(value = "/validated/{user}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TwitterosDB>> getvalidatedtwits(@PathVariable String user){
        List<TwitterosDB> response = twitterService.buscarValidados(user);
        return new ResponseEntity<List<TwitterosDB>>(response, HttpStatus.OK);
    }

    /**
     * Busca el top trending
     * @return lista trending 200 OK
     */
    @RequestMapping(value = "/trends", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> getTrends(){
        List<String> response = twitterService.buscarTrends();
        return new ResponseEntity<List<String>>(response, HttpStatus.OK);
    }
}