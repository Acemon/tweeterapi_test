package com.twitter.fernando.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import com.twitter.fernando.db.TwitterosDB;
import com.twitter.fernando.db.TwitterosRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TwitterServiceTest {

    @InjectMocks
    private TwitterServiceImpl twitterService;

    @Mock
    private TwitterosRepository twiterosRepo;

    @Test
    public void obtenerTweets(){
        List<TwitterosDB> twiterosList = obtenerList();
        Mockito.when(twiterosRepo.findAllTwitteros()).thenReturn(twiterosList);
        List<TwitterosDB> result = twitterService.obtenerTweets();
        Mockito.verify(twiterosRepo).findAllTwitteros();
        assertNotNull(result);
        assertEquals(result, twiterosList);
    }

    @Test
    public void buscarValidados(){
        String user = "";
        List<TwitterosDB> twiterosList = obtenerList();
        Mockito.when(twiterosRepo.findAllVerifiedUser(user)).thenReturn(twiterosList);
        List<TwitterosDB> result = twitterService.buscarValidados(user);
        Mockito.verify(twiterosRepo).findAllVerifiedUser(user);
        assertNotNull(result);
        assertEquals(result, twiterosList);
    }

    private List<TwitterosDB> obtenerList() {
        List<TwitterosDB> twiterosList = new ArrayList<>();
        TwitterosDB user = new TwitterosDB();
        user.setUsuario("usuario");
        user.setTexto("texto");
        user.setLocalizacion("localizacion");
        user.setValidacion(true);
        return twiterosList;
    }
}
